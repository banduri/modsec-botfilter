# Umgebung

Die Konfiguration fuer den apachen 2.4 und modsecurity 2.x ist gedacht um kleine 
und mittelgrosse Webseiten vor automatisierten Scripten die den Kontent abgreifen 
zu schuetzen. Allg. ist es kein Problem wenn Bots/Spiders auf der Webseite den 
Inhalt abgreifen respektive in ihren eigenen Index aufnehmen. Es ist jedoch ein 
Problem, wenn die Bots sich irgendwie in der Webseite verfangen, und mehrfach 
den gleichen Inhalt downloaden, an die Ressourcengrenze der Webseite stossen und 
so einen negativen Einfluss auf die Benutzerinnenerfahrung haben. 

## Vorraussetzung

Dies ist kein Tutorial zum installieren von modsecurity. Es wird davon ausgegangen, 
dass das module fuer den apache 2.4 Webserver in der Version 2.x installiert und 
geladen ist. Auch sollte ein Verstaendnis dafuer vorhanden sein wie die Webseite 
funktioniert und in den Logfiles des Webservers beobachtet worden sein welche 
Bots sich wo festfressen.

## Methodik

Das Sperren von Bots anhand von einzeln IPs oder Subnetmasken bedarf einer 
staendigen Pflege der Sperrlisten. Dieser Vorgang soll so weit es geht automatisiert 
werden. Dabei werden die Bots nicht durch ein Bandbreitenlimit ausgebremst, auch 
wenn das moeglich ist, sondern es wird mit dem HTTP-Status 429 (Ratelimit exceeded) 
vom Webserver geantwortet und kein Inhalt der Webanwendung ausgeliefert. Ferner wird 
zusaetzlich der HTTP-Header Retry-After gesetzt um die Bots darueber zu informieren, 
wann sie entsperrt werden. Das von den Bots erwartete Verhalten ist, dass sie mit dem 
sowohl mit dem Status-Code als auch mit dem Header etwas anfangen koennen und diese 
respektieren. Tun sie es nicht, verlaengert sich die Sperrfrist. Jeder Zugriff auf 
ausgewaehlte/gefaehrdete Ressourcen wird in einer Datenbank von modsecurity 
gespeichert (collection). Beim ueberschreiten des definierte Limits fuer eine 
Ressource wird die IP als Bot markiert und in einer zweiten collection abgelegt. Alle 
Eintrage die sich in der zweiten collection befinden werden auf HTTP-Ebene wie oben 
beschrieben blockiert. Dabei soll nicht nur die urspruengliche IP beruecksichtig 
werden, sondern auch der X-Forwarded-For Header falls er gesetzt ist um einen 
Betrieb hinter einem Proxy oder Cache zu erlauben.

# Die Configdatei

## Grundparameter

Modsecurity bietet ein paar Grundlegende Parameter die initial konfiguriert werden sollten. 

```
  SecRuleEngine On
  SecDebugLog /var/log/apache2/modsec.log
  SecDebugLogLevel 0
  # wir merken uns die sessions bis zu 3 Tage
  SecCollectionTimeout 259200
```

'Sessions' meint dabei nicht die Sessions der Webanwendung, sondern wie lange Eintrage 
in der modescurity-Datenbank vorgehalten wenn keine Zugriffe mehr statt finden. 

## Locations und VirtualHosts

Im Apachen lassen sich unterschiedliche ```<Location>``` und ```<LocationMatch>``` definieren. 
Diese werden bei Ueberlappung der Ressourcen in der Reihnfolge in der sie in der Konfig stehen 
zusammen gefuehrt  (sicher? Link?). 

## Basiskonfiguration mit Erklaerung

* erzeuge fuer alle Ressourcen die ```.php``` oder ```.py``` beinhalten einen Eintrag in der collection. 
* der Eintrag setzt sich aus dem Header X-Forwared-For, der IP und der angefragten Ressource zusammen
* Nenne diese collection ```ip```
* weisse dieser Regel die ID 7100 zu
* erlaube weiteres abarbeiten der Regeln (```pass```)
* fuehre diese Regel in phase 2 von modsecurity aus (gesamter Request mit body und header ist bekannt)

```
  <LocationMatch "(.php|.py)">
    # hashmap ueber x-forwarded-for,remote_ip und filepath erstellen
    # dabei verwenden wir spaeter einen counter in jeden eintrag
    SecAction "id:7100,phase:2,pass,initcol:ip=%{request_headers.x-forwarded-for}-%{REMOTE_ADDR}-%{REQUEST_FILENAME}"
```

* Initialisiere die Bot-collection mit X-Forwarded-For Header und IP
* nenne diese collection user

```
    SecAction "id:7101,phase:2,pass,initcol:user=%{request_headers.x-forwarded-for}-%{REMOTE_ADDR}"
```

* wenn in der ip-collection der counter-Eintrag bereits den Wert 750 ueberschritten hat, die einzelne Ressource 
... also 750 oder oefter von einer IP abgerufen wurde, markiere sie in der user-collection als Bot
... das wird durch eine 1 symbolisiert.
* erhoehe in der ip-collection den counter fuer den Zugriff auf diese Ressource

```
    SecRule IP:counter "@gt 750" "id:7102,phase:2,setvar:user.counter=1,pass"
    SecAction "id:7103,phase:2,pass,setvar:ip.counter=+1"
```

* wenn die Ressource zwischen 2:00 und 4:59 abgerufen wurde (Menschen sind da allg. weniger aktiv) 
... den Counter in der ip-collection um weitere 2 hoch zaehlen

```
    SecRule TIME_HOUR "^(02|03|04)$" "id:8104,phase:2,pass,setvar:ip.counter=+2"
```
* Zugriff blockieren und http-status 429 senden, wenn 750 Zugriffe auf eine Ressource erreicht wurden
* setzen der Apache-Request-Umgebungsvariable RATELIMITED um den Retry-Header ebenfalls sende zu koennen

```
    SecRule IP:counter "@gt 750" "id:8105,phase:2,deny,status:429,setenv:RATELIMITED"
```

* Sollte bereits eine andere Ressource als die auf die aktuell zugegriffen wird ueberschritten worden sein
... den Request ebenfalls blockieren. Nicht jede ressource darf 750 mal geladen werden, sondern solange
... bei keiner Ressource das Limit ueberschritten wurde ist alles OK, wurde es ueberschritten darf keine
... Ressource mehr geladen werden.
```
    SecRule USER:counter "@gt 0" "id:8106,phase:2,deny,status:429,setenv:RATELIMITED"
```

* Bots auch wieder entsperren
* alle 86400 Sekunden (24h) wird der ip-/Ressourcecounter um 200 Reduziert. Hat der Bot sich an das Limit
... gehalten, duerfen nun 200 weitere Zugriffe erfolgen. Wenn ein Bot also im Ratelimit drin ist, ist
... er dort fuer 24h drin.
* Das decrementieren des Zaehlers findet in der Log-Phase (5) des Apachen statt, wenn der Request 
... beantwortet wurde

```
    SecAction "phase:5,deprecatevar:ip.counter=200/86400,pass,id:8500"
```

* einmal pro tag vergessen wir alle Bots

```
    SecAction "phase:5,deprecatevar:user.counter=1/86400,pass,id:8501"
```

* Setzen des Retry-After Headers, um den Bot zu informieren, wann er wieder kommen soll
* 'nette'-Bots lassen sich so austricksen und ebenfalls sperren, wenn sie beispielsweise
... die Information bekommen, dass sie in 1 Minute wieder kommen sollen, der Zaehler fuer
... die Ressource allerdings erst in 24h reduziert wird. Die 'provozierten' Zugriffe 
... fuehren dazu, dass der Counter weiter hoch gezaehlt wird und somit auch die Reduktion
... um 200 alle 24h nicht ausreicht um den Bot zu entsperren

```
    #Header always set Retry-After "60" env=RATELIMITED
    Header always set Retry-After "86400" env=RATELIMITED

  </LocationMatch>
```

## Vollerblock

Die Basisconfig ist so ausgelegt, dass nur der Zugriff auf die rechenintensiven Ressourcen 
blockiert werden, da ein ausliefern von statischen Komponenten meist kein Problem ist. 
Wenn es jedoch gewuenscht ist laesst sich jedweder Zugriff mit einem 429 beantworten. 
Dies muss ebenfalls in einer ```<LocationMatch>``` erfolgen, denn wird dies direkt in 
einem ```<VirtualHost>```-Block gemacht, werden die Definitionen der ```<LocationMatch>``` 
nicht mehr ausgefuehrt und der ip-/Ressource-zaehler erreicht nur den maximalwert von 750 
unabhaengig davon wie oft der Bot wirklich zugreift.

```
  <LocationMatch "(?!.php)">
    SecAction "id:8106,phase:2,pass,initcol:user=%{request_headers.x-forwarded-for}"
    SecRule USER:counter "@gt 0" "id:8107,phase:2,deny,status:429,setenv:RATELIMITED"
    #Header always set Retry-After "60" env=RATELIMITED
    Header always set Retry-After "86400" env=RATELIMITED
  </LocationMatch>
```
